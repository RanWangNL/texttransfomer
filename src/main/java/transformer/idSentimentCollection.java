package transformer;

import java.util.ArrayList;

/**
 * Created by user on 31-5-16.
 */
public class idSentimentCollection {
    public idSentimentCollection(ArrayList<WordSentiment> result, Integer id) {
        this.result = result;
        this.id = id;
    }

    public Integer id;
    public ArrayList<WordSentiment> result;
}
